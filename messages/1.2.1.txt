Ver 1.2.1

Made class names searchable in Java and C# files.

Feedback is always welcome here:
https://bitbucket.org/rablador/whocalled/issues